<?php

namespace taktwerk\pages\blocks\controller;

use nullref\cms\components\Block as BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $controller;
    public $action;
    public $parameters;

    public function getName()
    {
        return 'Controller Action';
    }

    public function rules()
    {
        return [
            [['controller', 'action'], 'required'],
            [['parameters'], 'string']
        ];
    }

}