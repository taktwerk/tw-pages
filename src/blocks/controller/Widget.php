<?php

namespace taktwerk\pages\blocks\controller;

use nullref\cms\assets\owlCarousel\OwlCarousel2;
use taktwerk\pages\blocks\text\Widget as BaseWidget;
use yii\helpers\Html;
use Yii;
use Exception;
use yii\helpers\Url;

class Widget extends BaseWidget
{
    public $controller;
    public $action;
    public $parameters;

    /**
     * Calls an action. Make sure it's custom and return with renderAjax to avoid
     * faulty design problems.
     * @return null|string
     */
    public function run()
    {
        if (\Yii::$app->getUser()->isGuest && !$this->isPublic) {
            return '';
        }
        // Do your thing
        $pageId = $this->controller . '/' . $this->action;
        $url = Url::toRoute([$pageId]);
        try {
            $data = Yii::$app->runAction($pageId, $this->parameters);

            // Extract only content we want.
            // Todo: find a better technique
            //$data = preg_grep("`<div `")
            return $data;
        }
        catch(Exception $e) {
            return "the requested page '$pageId' does not exist.";
        }

        return null;
    }
}