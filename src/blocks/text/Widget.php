<?php
/**
 *
 */

namespace taktwerk\pages\blocks\text;

use nullref\cms\blocks\text\Widget as BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
    public $content;
    public $tag = '';
    public $tagClass = '';
    public $isPublic;
    public $isHidden;


    public function run()
    {
        if ($this->isHidden)
            return '';
        if (!empty($this->tag)) {
            return Html::tag($this->tag, $this->content, empty($this->tagClass) ? [] : ['class' => $this->tagClass]);
        }
        return $this->content;
    }
}