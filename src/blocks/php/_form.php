<?php

use taktwerk\pages\components\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\web\View;

/**
 * @var $form \yii\widgets\ActiveForm
 * @var $block \nullref\cms\blocks\html\Block
 * @var $this \yii\web\View
 */
list(, $footnotesUrl) = Yii::$app->assetManager->publish('@nullref/cms/assets/ckeditor-plugins/codemirror');
$this->registerJs("CKEDITOR.plugins.addExternal( 'codemirror', '" . $footnotesUrl . "/','plugin.js');
Object.keys(CKEDITOR.dtd.\$removeEmpty).forEach(function(key){CKEDITOR.dtd.\$removeEmpty[key] = 0;});
CKEDITOR.config.protectedSource.push(/<\\?[\\s\\S]*?\\?>/g);
", View::POS_END);

$editorConfig = [
    'id' => 'editor',
    'editorOptions' => [
        'startupMode' => 'source',
        'preset' => 'full',
        'removeButtons' => \Yii::$app->user->identity->isAdmin ? '' : 'Flash,Smiley,About,Form,Checkbox,Radio,Select,HiddenField,TextField,Textarea',
        'inline' => false,
        'extraPlugins' => 'codemirror',
        'allowedContent' => true,
        'basicEntities' => false,
        'entities' => false,
        'entities_greek' => false,
        'entities_latin' => false,
        'htmlEncodeOutput' => false,
        'entities_processNumerical' => false,
        'fillEmptyBlocks' => false,
        'fullPage' => false,
        'codemirror' => [
            'autoCloseBrackets' => false,
            'autoCloseTags' => false,
            'autoFormatOnStart' => false,
            'autoFormatOnUncomment' => false,
            'continueComments' => false,
            'enableCodeFolding' => false,
            'enableCodeFormatting' => false,
            'enableSearchTools' => false,
            'highlightMatches' => false,
            'indentWithTabs' => false,
            'lineNumbers' => false,
            'lineWrapping' => false,
            'mode' => 'application/x-httpd-php',
            'matchBrackets' => false,
            'matchTags' => false,
            'showAutoCompleteButton' => false,
            'showCommentButton' => false,
            'showFormatButton' => false,
            'showSearchButton' => false,
            'showTrailingSpace' => false,
            'showUncommentButton' => false,
            'styleActiveLine' => false,
            'theme' => 'default',
            'useBeautify' => false,
        ],
    ],
];

$editorConfig['editorOptions'] = ElFinder::ckeditorOptions('elfinder-backend', $editorConfig['editorOptions']);

echo $form->field($block, 'content')->widget(CKEditor::className(), $editorConfig);
echo $form->field($block, 'tag')->textInput();
