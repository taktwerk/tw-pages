<?php
/**
 *
 */

namespace taktwerk\pages\blocks\php;

use taktwerk\pages\blocks\text\Widget as BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
    public function run()
    {
        if ($this->isHidden)
            return '';
        if (\Yii::$app->getUser()->isGuest && !$this->isPublic) {
            return '';
        }
        ob_start();
        eval($this->content);
        if (!empty($this->tag)) {
            return Html::tag($this->tag, ob_get_clean(), ['class' => $this->tagClass]);
        }
        return ob_get_clean();
    }
}