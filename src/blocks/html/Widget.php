<?php
/**
 *
 */

namespace taktwerk\pages\blocks\html;

use taktwerk\pages\blocks\text\Widget as BaseWidget;
use yii\helpers\Html;


class Widget extends BaseWidget
{
    public function run()
    {
        if ($this->isHidden)
            return '';
        if (\Yii::$app->getUser()->isGuest && !$this->isPublic) {
            return '';
        }
        ob_start();
        $this->content = preg_replace_callback('#/[a-z]{2}+/elfinder-backend/connect\?_csrf=.*target=fls[0-9]_(.*)"#iU', function ($matches) {
            //Only replace jpg, jpeg and png files with direct link to assets, since for ie. pdf files access if forbidden on S3
            if (strpos(base64_decode($matches[1]),'.jpg') !== false || strpos(base64_decode($matches[1]),'.jpeg') !== false || strpos(base64_decode($matches[1]),'.png') !== false) {
                return '<?php echo \taktwerk\yiiboilerplate\components\Helper::getImageUrl(base64_decode("' . $matches[1] . '"))?>"';
            }
            return $matches[0];
        },$this->content);
        // Change linked stuffs to go thru elfinder controller which is opened for un-logged users
        $this->content = str_replace('elfinder-backend', 'elfinder', $this->content);
        eval(' ?>' . $this->content . '<?php ');

        if (!empty($this->tag)) {
            return Html::tag($this->tag, ob_get_clean(), ['class' => $this->tagClass]);
        }
        return ob_get_clean();
    }
}