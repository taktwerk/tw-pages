<?php

namespace taktwerk\pages\blocks\html;

use nullref\cms\components\Block as BaseBlock;

/**
 * Class Block
 */
class Block extends BaseBlock
{
    public $content;
    public $tag = 'div';
    public $tagClass = '';

    public function getName()
    {
        return 'HTML Block';
    }

    public function rules()
    {
        return [
            [['content', 'tag'], 'safe'],
            [['tagClass'], 'string'],
        ];
    }
}
