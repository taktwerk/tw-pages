<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace dmstr\modules\pages\controllers;

use dmstr\modules\pages\models\Tree;
use yii\web\Controller;

/**
 * Class TestController.
 *
 * @author $Author
 */
class TestController extends Controller
{
    public function actionIndex()
    {
        $tree = Tree::getMenuItems(Tree::ROOT_NODE_PREFIX);

        return $this->render('index', ['tree' => $tree]);
    }
}
