<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages\controllers\api;

use dmstr\modules\pages\controllers\api\DefaultController as BaseController;

/**
 * This is the class for REST controller "DefaultController".
 *
 */
class DefaultController extends BaseController
{
    public $modelClass = 'taktwerk\pages\models\Tree';
}
