<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages\controllers;

use taktwerk\pages\models\Tree;
use dmstr\modules\pages\controllers\DefaultController as BaseController;
use yii\helpers\Url;
use yii\web\HttpException;
use Yii;

/**
 * Class DefaultController.
 *
 */
class DefaultController extends BaseController
{
    /**
     * renders a page view from the database.
     *
     * @param $pageSlug
     * @param null $parentLeave
     *
     * @return string
     *
     * @throws HttpException
     */
    public function actionPage($pageName = null, $pageSlug = null, $parentLeave = null)
    {
        Url::remember();
        Yii::$app->session['__crudReturnUrl'] = null;
        // Set layout
        $this->layout = '@app/views/layouts/main';
        // Get active Tree object, allow access to invisible pages
        // @todo: improve handling, using also roles

        $pageQuery = Tree::find()->where(
            [
                Tree::ATTR_SLUG => $pageSlug,
                Tree::ATTR_ACTIVE => Tree::ACTIVE,
                Tree::ATTR_ACCESS_DOMAIN => Yii::$app->language,
            ]
        );
        // Show disabled pages for admins
        if (!Yii::$app->user->can('pages')) {
            $pageQuery->andWhere(
                [
                    Tree::ATTR_DISABLED => Tree::NOT_DISABLED,
                ]
            );
        }
        // get page
        $page = $pageQuery->one();

        if ($page !== null) {
            // Access ?
            if ($page->hasAccess(true)) {
                // Set page title, use name as fallback
                $this->view->title = $page->page_title ?: $page->name;
                // Register default SEO meta tags
                $this->view->registerMetaTag(['name' => 'keywords', 'content' => $page->default_meta_keywords]);
                $this->view->registerMetaTag(['name' => 'description', 'content' => $page->default_meta_description]);
                // Render view
                return $this->render($page->view, ['page' => $page]);
            }
            else {
                return $this->redirect('/');
            }
        } else {
            // Didn't find anything? Maybe we can find something in another language
            return $this->findEquivalent($pageName, $pageSlug, $parentLeave);
        }
    }

    /**
     * @param null $pageName
     * @param null $pageSlug
     * @param null $parentLeave
     * @return \yii\web\Response
     * @throws HttpException
     */
    protected function findEquivalent($pageName = null, $pageSlug = null, $parentLeave = null)
    {
        // Try and find the slug we're requesting in another lanauge
        $page = $this->findPageWithSlug($pageSlug);

        // Found the slug in another language
        if ($page !== null) {
            $pageInOurLang = $this->findPageWithDomainID($page->domain_id);
            if ($pageInOurLang !== null) {
                // Found the same domain id but in our lang, redirect there properly.
                return $this->redirect(Url::to($pageInOurLang->createRoute()));
            }
        }

        // Nothing, redirect to route page
        return $this->redirect('/');
    }

    /**
     * Find a page with slug, whatever the language
     * @param $slug
     * @return mixed
     */
    protected function findPageWithSlug($slug)
    {
        $pageQuery = Tree::find()->where(
            [
                Tree::ATTR_SLUG => $slug,
                Tree::ATTR_ACTIVE => Tree::ACTIVE,
            ]
        );
        // Show disabled pages for admins
        if (Yii::$app->user->can('pages')) {
            $pageQuery->andWhere(
                [
                    Tree::ATTR_DISABLED => Tree::NOT_DISABLED,
                ]
            );
        }
        return $pageQuery->one();
    }

    /**
     * Find a page with a domain ID in our lanauge
     * @param $domainId
     * @return mixed
     */
    protected function findPageWithDomainID($domainId)
    {
        // Now find for the same domainid in our language
        $pageQuery = Tree::find()->where(
            [
                Tree::ATTR_DOMAIN_ID => $domainId,
                Tree::ATTR_ACTIVE => Tree::ACTIVE,
                Tree::ATTR_ACCESS_DOMAIN => Yii::$app->language,
            ]
        );
        // Show disabled pages for admins
        if (Yii::$app->user->can('pages')) {
            $pageQuery->andWhere(
                [
                    Tree::ATTR_DISABLED => Tree::NOT_DISABLED,
                ]
            );
        }
        return $pageQuery->one();
    }
}
