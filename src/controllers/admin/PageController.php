<?php

namespace taktwerk\pages\controllers\admin;

use nullref\cms\controllers\admin\PageController as BasePageController;
use taktwerk\pages\models\Page;
use nullref\cms\models\Block;
use nullref\cms\models\PageHasBlock;
use taktwerk\pages\models\PageHasCmsPage;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * BlockController implements the CRUD actions for Block model.
 */
class PageController extends BasePageController
{
    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var Page $model */
        $model = Yii::createObject(Page::className());

        if ($model->loadWithRelations(Yii::$app->request->post()) && $model->save()) {
            if (isset($_POST["submit-back"])) {
                $page = PageHasCmsPage::findOne(['cms_page_id' => $model->id]);
                return $this->redirect(["/pages", 'id' => $page->page_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $default = Yii::$app->request->isPost ? ['PageHasBlock' => []] : [];
        if ($model->loadWithRelations(array_merge($default, Yii::$app->request->post())) && $model->save()) {
            Yii::$app->cache->delete('cms.page.' . $model->route);

            if (isset($_POST["submit-back"])) {
                $page = PageHasCmsPage::findOne(['cms_page_id' => $model->id]);
                return $this->redirect(["/pages/", 'id' => $page->page_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionConfig($id = null)
    {
        /** @var Block $model */
        if ($id === null) {
            $model = Yii::$app->session->get('new-block');
            if (!$model) {
                $this->redirect('create');
            }
        } else {
            $model = $this->findModel($id);
        }
        /** @var \nullref\cms\components\BlockManager $blockManager */
        $blockManager = Yii::$app->getModule('cms')->get('blockManager');

        /** @var \nullref\cms\components\Block $block */
        $block = $blockManager->getBlock($model->class_name);

        if (!$model->isNewRecord) {
            $block->setAttributes($model->getData());
        }

        if ($block->load(Yii::$app->request->post()) && ($block->validate())) {
            $model->setData($block);
            $model->save();
            Yii::$app->session->remove('new-block');

            /** Create relation when path page_id parameter */
            if ($pageId = Yii::$app->request->get('page_id')) {
                if ($id === null) {
                    $pageHasBlock = new PageHasBlock(['page_id' => $pageId, 'block_id' => $model->id]);
                    $pageHasBlock->save(false, ['page_id', 'block_id']);
                }

                if (isset($_POST["submit-back"])) {
                    return $this->redirect(['/cms/admin/page/update', 'id' => $pageId]);
                } else {
                    return $this->redirect(["/pages/admin/block/config", 'id' => $model->id, 'page_id' => $pageId]);
                }
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('config', [
                'block' => $block,
            ]);
        }
        return $this->render('config', [
            'block' => $block,
        ]);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
