<?php

namespace taktwerk\pages\controllers\admin;

use nullref\cms\controllers\admin\BlockController as BaseBlockController;
use taktwerk\pages\models\Block;
use nullref\cms\models\PageHasBlock;
use nullref\core\interfaces\IAdminController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * BlockController implements the CRUD actions for Block model.
 */
class BlockController extends BaseBlockController
{
    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionConfig($id = null)
    {
        /** @var Block $model */
        if ($id === null) {
            $model = Yii::$app->session->get('new-block');
            if (!$model) {
                $this->redirect('create');
            }
        } else {
            $model = $this->findModel($id);
        }
        /** @var \nullref\cms\components\BlockManager $blockManager */
        $blockManager = Yii::$app->getModule('cms')->get('blockManager');

        /** @var \nullref\cms\components\Block $block */
        $block = $blockManager->getBlock($model->class_name);

        if (!$model->isNewRecord) {
            $block->setAttributes($model->getData());
        }

        if ($block->load(Yii::$app->request->post()) && ($block->validate())) {
            $model->setData($block);
            $model->save();
            Yii::$app->session->remove('new-block');

            /** Create relation when path page_id parameter */
            if ($pageId = Yii::$app->request->get('page_id')) {
                if ($id === null) {
                    $pageHasBlock = new PageHasBlock(['page_id' => $pageId, 'block_id' => $model->id]);
                    $pageHasBlock->save(false, ['page_id', 'block_id']);
                }

                if (isset($_POST["submit-back"])) {
                    return $this->redirect(['/pages/admin/page/update', 'id' => $pageId]);
                }
                else {
                    return $this->redirect(["/pages/admin/block/config", 'id' => $model->id, 'page_id' => $pageId]);
                }
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('config', [
                'block' => $block,
            ]);
        }
        return $this->render('config', [
            'block' => $block,
        ]);
    }

}
