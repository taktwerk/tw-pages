<?php
// Load the containers of the page
$containers = $page->cmsPages;

foreach($containers as $container):
    $model = $container->cmsPage;
    ?>
    <?php foreach ($model->items as $item): ?>
    <div class="page-block">
        <input type="hidden" name="PageHasBlock[<?= $item->id ?>][block_id]"
               value="<?= $item->block_id ?>">
        <input type="hidden" name="PageHasBlock[<?= $item->id ?>][order]"
               value="<?= $item->order ?>">
        <?php if ($item->block): ?>
            <?= $item->block->getWidget() ?>
        <?php else: ?>
            <?= EmptyBlock::widget(['id' => $item->block_id]) ?>
        <?php endif ?>
    </div>
    <?php endforeach ?>
<?php endforeach ?>