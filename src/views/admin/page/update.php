<?php

use yii\helpers\Html;
use taktwerk\pages\models\PageHasCmsPage;

/* @var $this yii\web\View */
/* @var $model nullref\cms\models\Page */

$this->title = Yii::t('cms', 'Updating Container');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cms', 'Pages'), 'url' => ['/pages']];
$this->params['breadcrumbs'][] = Yii::t('cms', 'Update');

$page = PageHasCmsPage::findOne(['cms_page_id' => $model->id]);
?>
<div class="page-update">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?php if ($page): ?>
            <?= Html::a(Yii::t('cms', 'Back to Pages'), ['/pages', 'id' => $page->page_id], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
