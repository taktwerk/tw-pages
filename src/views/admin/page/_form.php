<?php

use mihaildev\ckeditor\CKEditor;
use nullref\cms\assets\PageFormAssets;
use nullref\cms\models\Page;
use unclead\widgets\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use taktwerk\pages\models\PageHasCmsPage;

/* @var $this yii\web\View */
/* @var $model nullref\cms\models\Page */
/* @var $form yii\widgets\ActiveForm */

PageFormAssets::register($this);

$pageTypesMap = Page::getTypesMap();

/** @var \nullref\cms\components\PageLayoutManager $layoutManager */
$layoutManager = Yii::$app->getModule('cms')->get('layoutManager');

$cmsPage = PageHasCmsPage::findOne(['cms_page_id' => $model->id]);
if ($cmsPage) {
    $parentPage = $cmsPage->page;
}

if (empty($model->type)) {
    $model->type = Page::TYPE_BLOCKS;
}
?>
<div class="hide">
    <li class="list-group-item" id="pageItemTmpl">
        <button type="button"
                class="btn btn-danger btn-xs"
                data-action="remove-block"
                data-id="">
            <i class="fa fa-close"></i>
        </button>
        <input type="hidden" name="PageHasBlock[:id][block_id]" value=":block_id">
        <input type="hidden" name="PageHasBlock[:id][order]" value=":order">
        :name
        <i class="fa fa-bars pull-right"></i>
    </li>
</div>

<div class="page-form">

    <?php $form = ActiveForm::begin(['encodeErrorSummary' => false]); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->errorSummary($model) ?>
        </div>
        <div class="col-md-3">
            <input type="hidden" name="Page[title]" value="<?=$model->title?>">

            <?php if($parentPage): ?>
                <div class="form-group field-page-type">
                    <label class="control-label" for="page-page">Page</label>
                    <input type="text" id="page-page" class="form-control" disabled name="page-parent-page" value="<?=$parentPage->name ?> (#<?=$parentPage->id?>)" />
                    <div class="help-block"></div>
                </div>
            <?php endif; ?>
        </div>

        <div class="col-md-9">

            <div class="row">
                <?= $this->render('_blocks', [
                    'model' => $model,
                ]) ?>
            </div>

        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('cms', 'Create') : Yii::t('cms', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::submitButton(Yii::t('cms', 'Save &amp; Back'), ['class' => 'btn btn-primary', 'name' => 'submit-back']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
