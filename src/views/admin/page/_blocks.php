<?php

use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;
use nullref\cms\models\Block as BlockModel;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $model nullref\cms\models\Page */
/* @var BlockModel[] $blocks */
$blocks = BlockModel::find()->indexBy('id')->all();

?>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Yii::t('cms', 'Page Content') ?>
        </div>
        <ul class="list-group page-items-list" id="pageItemsList">
            <?php foreach ($model->items_list as $item): ?>
                <li class="list-group-item">
                    <?= Html::a(
                        FA::i(FA::_PENCIL),
                        ['/pages/admin/block/config', 'id' => $item->block_id, 'page_id' => $model->id],
                        ['class' => 'btn btn-xs btn-primary']
                    ) ?>

                    <?= Html::a(
                        FA::i(FA::_COG),
                        ['/pages/admin/block/update', 'id' => $item->block_id, 'page_id' => $model->id],
                        ['class' => 'btn btn-xs btn-success']
                    ) ?>

                    <button type="button"
                            class="btn btn-danger btn-xs"
                            data-action="remove-block"
                            data-id="">
                        <i class="fa fa-close"></i>
                    </button>


                    <?php if ($item->block): ?>
                        <i class="fa fa-<?= $item->block->isPublic() ? FA::_EYE : FA::_EYE_SLASH ?>"></i>
                    <?php endif ?>

                    <input type="hidden" name="PageHasBlock[<?= $item->id ?>][block_id]"
                           value="<?= $item->block_id ?>">
                    <input type="hidden" name="PageHasBlock[<?= $item->id ?>][order]"
                           value="<?= $item->order ?>">

                    <?= $item->block ? $item->block->getTypeName() . ' - ' . $item->block->getFullName() : Yii::t('cms', 'Block "{id}" not found', ['id' => $item->block_id]) ?>
                    <i class="fa fa-bars pull-right"></i>
                </li>
            <?php endforeach ?>
        </ul>
        <?php if (!$model->isNewRecord): ?>
            <div class="panel-footer">
                <?= Html::a(Yii::t('cms', 'Add new block'), ['/pages/admin/block/create', 'page_id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
        <?php endif ?>
    </div>
</div>

<div class="col-md-12">
    <?php
    $items = [];
    foreach ($blocks as $id => $block) {
        $items[] =  "
    <button type=\"button\"
        class=\"btn btn-primary btn-xs\"
        data-action=\"add-block\"
        data-id=\"$id\"
        data-name=\"" . $block->getTypeName() . ' - ' . $block->getFullName() . "\">
        <i class=\"fa fa-plus\"></i>
    </button> " . $block->getTypeName() . ' - ' . $block->getFullName();
    }
    echo Collapse::widget([
        'items' => [
            [
                'label' => Yii::t('cms', 'Existing Blocks'),
                'content' => $items,
                'options' => [
                    'id' => "blocksList"
                ]
            ]
        ]
    ]);
    ?>
</div>


