<?php

use yii\helpers\Html;
use nullref\cms\models\Page;
use taktwerk\pages\models\PageHasCmsPage;


/* @var $this yii\web\View */
/* @var $model nullref\cms\models\Block */

$this->title = Yii::t('cms', 'Create Block');
$this->params['breadcrumbs'][] = $this->title;

// If we have a page_id, get it's name to pre-generate the block data
if ($pageId = Yii::$app->request->get('page_id')) {
    // Get the cms page
    //$pageCms = Page::findOne(['id' => $pageId]);
    if ($link = PageHasCmsPage::findOne(['cms_page_id' => $pageId])) {
        $pageTw = $link->page;
        $model->id = $pageTw->name_id . '-' . date('YmdHis');
        $model->name = $pageTw->name_id;
        $model->visibility = \nullref\cms\models\Block::VISIBILITY_PUBLIC; // Default visibility to public
    }
}
$model->class_name = 'html';
?>
<div class="block-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
