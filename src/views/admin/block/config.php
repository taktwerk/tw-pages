<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $block \nullref\cms\components\Block
 */
$this->title = Yii::t('cms', 'Config Block ({name})', ['name' => $block->getName()]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-config">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($block) ?>

    <?= $this->renderFile($block->getForm(), ['form' => $form, 'block' => $block]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cms', 'Save'), ['class' => 'btn btn-primary', 'name' => 'submit-default']) ?>
        <?= Html::submitButton(Yii::t('cms', 'Save &amp; Back'), ['class' => 'btn btn-primary', 'name' => 'submit-back']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
