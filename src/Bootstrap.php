<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages;

use Yii;
use yii\base\Application;
use dmstr\modules\pages\Bootstrap as BaseBootstrap;

/**
 * Class Bootstrap
 * @package taktwerk\pages
 */
class Bootstrap extends BaseBootstrap
{
    /**
     * Bootstrap the module to Yii
     * @param Application $app
     */
    public function bootstrap($app)
    {
        parent::bootstrap($app);

        // register migration
        $app->params['yii.migrations'][] = '@vendor/taktwerk/tw-pages/src/migrations';
        //$app->params['yii.migrations'][] = '@vendor/nullref/yii2-cms/src/migrations'; -> don't use them anymore. They are namespaced and that doesn't work with dmstr/yii2-migrate-command.

        // Tell where to search for cms-views
        $view = Yii::$app->getView();
        $view->theme->pathMap["@nullref/cms/views/admin"] = '@vendor/taktwerk/tw-pages/src/views/admin';
    }
}
