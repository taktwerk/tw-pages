<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages\assets;

use dmstr\modules\pages\assets\PagesAsset as BaseBundle;

/**
 * @link http://www.diemeisterei.de/
 *
 * @copyright Copyright (c) 2015 diemeisterei GmbH, Stuttgart
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class PagesAsset extends BaseBundle
{
    public $depends = ['yii\bootstrap\BootstrapAsset'];
}
