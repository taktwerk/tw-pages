<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages;

use dmstr\modules\pages\Module as BaseModule;

/**
 * Class Module.
 *
 */
class Module extends BaseModule
{
    /**
     * Version of the module
     */
    const VERSION = '1.1.31';

    /**
     * @var string Route suffix for building the urls
     */
    public $routePrefix = '';

    /**
     * @var string to tell there is a current page slug even when outside of a page controller
     */
    public $forcePageSlug = null;

    /**
     * @var bool allows to force the second level menu not to show from a specific page or controller
     */
    public $forceRemoveSubmenu = false;
}
