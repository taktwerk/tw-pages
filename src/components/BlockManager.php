<?php
/**
 * Taktwerk Pages module, extends dmstr/pages and nullref/cms.
 */
namespace taktwerk\pages\components;

use nullref\cms\components\BlockManager as BaseBlockManager;
use taktwerk\pages\models\Block as BlockModel;
use yii\caching\DbDependency;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

class BlockManager extends BaseBlockManager
{

    /**
     * List of available Block Types
     * @return mixed
     */
    public function getList()
    {
        return [
            'html' => 'taktwerk\pages\blocks\html',
            'controller' => 'taktwerk\pages\blocks\controller',
            'php' => 'taktwerk\pages\blocks\php',
        ];
    }

    /**
     * @param $id
     * @param array $config
     */
    public function getWidget($id, $config = [])
    {
        /** @var BlockModel $block */
        $dependency = new DbDependency();
        $dependency->sql = 'SELECT MAX(updated_at) FROM ' . BlockModel::tableName();
//        $block = BlockModel::getDb()->cache(function () use ($id) {
//            return BlockModel::find()->where(['id' => $id])->one();
//        }, null, new TagDependency(['tags' => 'cms.block.' . $id]));
        $block = BlockModel::getDb()->cache(function () use ($id) {
            return BlockModel::find()->where(['id' => $id])->one();
        }, null, $dependency);
        if ($block) {
            $config = ArrayHelper::merge($config, $block->getData(), ['isPublic' => $block->isPublic(), 'isHidden' => $block->isHidden()]);
            $config['class'] = $this->getList()[$block->class_name] . self::CLASS_WIDGET;
        } else {
            return false;
            $config = [
                'class' => $this->emptyBlockClass,
                'id' => $id,
            ];
        }
        /** @var \nullref\cms\components\Widget $widget */
        $widget = \Yii::createObject($config);
        $blockObj = $this->getBlock($block->class_name, $block->getData());
        $blockObj->id = $block->id;
        $widget->setBlock($blockObj);
        return $widget;
    }
}
