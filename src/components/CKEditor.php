<?php
/**
 * Date: 17.01.14
 * Time: 1:18
 */

namespace taktwerk\pages\components;

use yii\helpers\ArrayHelper;
use yii\web\View;
use mihaildev\ckeditor\CKEditor as BaseCKeditor;

class CKEditor extends BaseCKeditor{

    public function run()
    {
        list(, $assetPath) = \Yii::$app->assetManager->publish('@vendor/mihaildev/yii2-ckeditor/editor');
        $this->view->registerJs("window['CKEDITOR_BASEPATH'] = '$assetPath/';", View::POS_BEGIN);
        parent::run();
    }
} 