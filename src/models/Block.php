<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 12/21/2016
 * Time: 7:08 PM
 */

namespace taktwerk\pages\models;

use \nullref\cms\models\Block as BaseBlock;
use yii\helpers\ArrayHelper;

class Block extends BaseBlock
{
    /**
     * Visibility levels
     */
    const VISIBILITY_HIDDEN = 3;

    /**
     * @return mixed
     */
    public static function getVisibilityList()
    {
        return ArrayHelper::merge(
            parent::getVisibilityList(),
            [
                self::VISIBILITY_HIDDEN => \Yii::t('cms', 'Hidden visibility'),
            ]
        );
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return $this->visibility === self::VISIBILITY_HIDDEN;
    }
}
