<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages\models;

use dektrium\rbac\validators\RbacValidator;
use taktwerk\yiiboilerplate\helpers\DebugHelper;
use Yii;
use dmstr\modules\pages\models\Tree as BaseTree;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Json;
use nullref\cms\models\Page as CmsPage;

/**
 * This is the tree model class, extended from \kartik\tree\models\Tree.
 *
 * @property string $name
 * @property string $page_title
 * @property string $name_id
 * @property string $domain_id
 * @property string $slug
 * @property string $route
 * @property array $roles
 * @property array $page_role
 * @property string $view
 * @property string $default_meta_keywords
 * @property string $default_meta_description
 * @property string $request_params
 * @property int $access_owner
 * @property string $access_domain
 * @property string $access_read
 * @property string $access_update
 * @property string $access_delete
 * @property string $created_at
 * @property string $updated_at
 */
class Tree extends BaseTree
{
    /**
     * Attribute names.
     */
    const ATTR_SLUG = 'slug';

    public $page_roles;

    public $module;

    /**
     * Cached current tree item
     * @var bool
     */
    protected static $cachedCurrentItem = null;

    /**
     * Cache for parent children
     * @var array
     */
    protected static $cachedChildRoute = [];

    /**
     * @var \yii\caching\Cache
     */
    protected static $cache = null;

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        // Slug isn't required, but if there is none, sluggify the name.
        if (empty($this->slug)) {
            $this->slug = Inflector::slug($this->name);
        }

        // Allow special stuff in the name (like & and ' )
        $this->name = \yii\helpers\Html::decode($this->name);

        return parent::beforeSave($insert);
    }

    /**
     * Make sure to load the roles from the form, since it's a privot table.
     */
    public function beforeValidate()
    {
        $treeRoles = Yii::$app->request->post('TreeRoles');
        $this->page_roles = $treeRoles['roles'];

        return parent::beforeValidate();
    }

    /**
     * Label attribution
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'page_roles' => Yii::t('rbac', 'Roles'),
            ]
        );
    }

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [
                    'page_roles', RbacValidator::className()
                ]
            ]
        );
    }

    /**
     * Get the roles associated to this tree.
     * @return mixed
     */
    public function getRoles()
    {
        $roles = [];
        foreach ($this->pageRoles as $role) {
            $roles[] = $role->role;
        }
        return array_values($roles);
    }

    /**
     * Extend afterSave to create pivoting data required.
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        // Only try when updating. The node is created first to attach it to a parent, and later updated
        if (!$insert) {
            // If we don't have any cms pages attached, create on after saving the Tree.
            $container = $this->cmsPages;
            if (count($container) == 0) {
                $cms = new CmsPage;
                $cms->route = $this->domain_id;
                $cms->title = $this->name;
                $cms->layout = '@nullref/cms/views/layouts/clear';

                // We might not be able to save the cmsPage, because
                // the original page doesn't have a slug or a name.
                if ($cms->save()) {
                    $pivot = new PageHasCmsPage;
                    $pivot->page_id = $this->id;
                    $pivot->cms_page_id = $cms->id;
                    $pivot->save();
                }
            } elseif (count($container) > 1) {
                // We have too many pages, we need to clean some up.
                foreach ($container as $page) {
                    $cmsPage = $page->cmsPage;
                    if (empty($cmsPage)) {
                        $page->delete();
                    }
                }
            }
        }

        // Update roles
        $this->updateRoles();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param array $additionalParams
     *
     * @return null|string
     */
    public function createRoute($additionalParams = [])
    {
        // If we have a direct tag in the slug, route to it directly
        if (strpos(strtolower($this->slug), '{direct}') !== false) {
            $language = Yii::$app->language;
            $route = str_ireplace('{direct}', null, "/$language/" . $this->slug);
            return $route;
        }

        $mySlug = !empty($this->slug) ? $this->slug : Inflector::slug($this->name);

        // If it's a level 2 and has children, use the first child instead for the route.
        if ($this->lvl == 2) {
            /*if (isset(self::$cachedChildRoute[$this->id])) {
                return self::$cachedChildRoute[$this->id];
            }*/

            $query = $this->children()->with('pageRoles');
            $key = md5($query->createCommand()->getRawSql());
            $children = self::getCache()->get($key);

            if ($children === false) {
                $children = $query->all();
                self::getCache()->set($key, $children, 0, self::getDependency());
            }

            if (count($children) > 0) {
                self::$cachedChildRoute[$this->id] = null;
                foreach ($children as $child) {
                    if ($child->hasAccess()) {
                        return self::$cachedChildRoute[$this->id] = $child->createRoute();
                    }
                }
                return self::$cachedChildRoute[$this->id];
            }
        }

        $prefix = $this->optsRoutePrefix();
        $route = '/';

        if (!empty($prefix)) {
            $route .= $prefix . '/';
        }

        $slugFolder = $this->resolvePagePath(true);
        if (!empty($slugFolder)) {
            $route .= $slugFolder . '/';
        }

        // Finish building the route
        $route .= $mySlug;
        $route = [$route];

        if (Json::decode($this->request_params)) {
            $route = ArrayHelper::merge($route, Json::decode($this->request_params));
        }

        if (!empty($additionalParams)) {
            $route = ArrayHelper::merge($route, $additionalParams);
        }

        return $route;
    }

    /**
     * Get Route Prefix
     *
     * @return array list of options
     */
    public static function optsRoutePrefix()
    {
        return \Yii::$app->getModule('pages')->routePrefix;
    }

    /**
     * @param bool $activeNode
     * @return null|string
     */
    protected function resolvePagePath($activeNode = false)
    {
        // return no path for root nodes
        $query = $this->parents(1);
        $key = md5($query->createCommand()->getRawSql());
        $parent = self::getCache()->get($key);
        if ($parent === false) {
            $parent = $query->one();
            self::getCache()->set($key, $parent, 0, self::getDependency());
        }
        if (!$parent) {
            return null;
        }
        // return no path for first level nodes
        if ($activeNode && $parent->isRoot()) {
            return null;
        }
        if (!$activeNode && $parent->isRoot()) {
            // start-point for building path
            $path = $this->slug ?: Inflector::slug($this->page_title ?: $this->name);
        } elseif (!$activeNode) {
            // if not active, build up path
            $path = $parent->resolvePagePath(false) . '/' .
                $this->slug ?: Inflector::slug($this->page_title ?: $this->name);
        } elseif ($activeNode && !$parent->isRoot()) {
            // building path finished
            $path = $parent->resolvePagePath(false);
        } else {
            $path = null;
        }
        return $path;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmsPages()
    {
        return $this->hasMany(PageHasCmsPage::className(), ['page_id' => 'id']);
    }

    /**
     * Get breadcrumbs dependent on current page
     * @param $maxLevel
     * @param Tree|null $page
     * @return bool
     */
    public static function getSubmenu($maxLevel = 3, Tree $page = null)
    {
        // Submenu can be forced off from controller.
        if (Yii::$app->getModule('pages')->forceRemoveSubmenu) {
            return false;
        }

        if ($page === null) {
            $page = Tree::getCurrentItem();
        }

        // If we have a valid page found, get its submenu
        if ($page) {
            $children = Tree::getMenuItems($page->domain_id, $maxLevel, false, true);
            if (!empty($children)) {
                return $children;
            } // If we are on a max level page, get our parent's submenu
            elseif ($page->lvl == $maxLevel) {
                $query = $page->parents(1);
                $key = md5($query->createCommand()->getRawSql());
                $parent = self::getCache()->get($key);
                if ($parent === false) {
                    $parent = $query->one();
                    self::getCache()->set($key, $parent, 0, self::getDependency());
                }
                return self::getSubmenu($maxLevel, $parent);
            }
        }

        return false;
    }

    /**
     * Get the current item based on the page slug
     * @return bool|null|static
     */
    public static function getCurrentItem()
    {
        if (self::$cachedCurrentItem === null) {
            self::$cachedCurrentItem = self::detectCurrentItem();
        }

        return self::$cachedCurrentItem;
    }

    /**
     * Detect the current item based on page slug and other methods. For better performances,
     * cache the result of this call.
     * @return bool|null|static
     */
    protected static function detectCurrentItem()
    {
        $slug = Yii::$app->request->get('pageSlug');

        // No slug? Might have a $forcePageSlug on the module set from somewhere else
        if (empty($slug)) {
            $slugOverride = Yii::$app->getModule('pages')->forcePageSlug;
            if (!empty($slugOverride)) {
                $slug = $slugOverride;
            }
        }
        if (!empty($slug)) {
            $query = Tree::find()->andWhere(['slug' => $slug, 'access_domain' => mb_strtolower(\Yii::$app->language)]);
            $key = md5($query->createCommand()->getRawSql());
            $model = self::getCache()->get($key);
            if ($model === false) {
                $model = $query->one();
                self::getCache()->set($key, $model, 0, self::getDependency());
            }
            if ($model) {
                return $model;
            }
        }

        // Doesn't have a slug, maybe it's a direct route that has a page
        $route = Yii::$app->request->resolve();
        if (!empty($route)) {
            if (!empty($route[0])) {
                $slug = "{direct}" . $route[0];
                $query = Tree::find()->andWhere(['slug' => $slug, 'access_domain' => mb_strtolower(\Yii::$app->language)]);
                $key = md5($query->createCommand()->getRawSql());
                $model = self::getCache()->get($key);
                if ($model === false) {
                    $model = $query->one();
                    self::getCache()->set($key, $model, 0, self::getDependency());
                }
                if ($model) {
                    return $model;
                }
            } // Still nothing? Let's just check if we are root, which is just a {direct} link.
            else {
                $query = Tree::find()->andWhere(['slug' => '{direct}', 'access_domain' => mb_strtolower(\Yii::$app->language)]);
                $key = md5($query->createCommand()->getRawSql());
                $model = self::getCache()->get($key);
                if ($model === false) {
                    $model = $query->one();
                    self::getCache()->set($key, $model, 0, self::getDependency());
                }
                if ($model) {
                    return $model;
                }
            }
        }

        return false;
    }

    /**
     * Get active and visible menu nodes for the current application language
     *
     * @param $domainId integer the domain id of the root node
     * @param $maxLevel integer the maximum level for a page to be shown
     * @param bool $checkUserPermissions
     * @param bool $forceExactLevel
     * @return array
     */
    public static function getFilteredMenuItems(
        $domainId,
        $maxLevel = 2,
        $checkUserPermissions = false,
        $forceExactLevel = false
    ) {
        // Get root node by domain id
        $rootCondition['domain_id'] = $domainId;
        $rootCondition['access_domain'] = [self::GLOBAL_ACCESS_DOMAIN, mb_strtolower(\Yii::$app->language)];
        if (!Yii::$app->user->can('pages')) {
            $rootCondition[self::ATTR_DISABLED] = self::NOT_DISABLED;
        }
        $key = md5(serialize($rootCondition));
        $rootNode = self::getCache()->get($key);
        if ($rootNode === false) {
            $rootNode = self::findOne($rootCondition);
            self::getCache()->set($key, $rootNode, 0, self::getDependency());
        }

        if ($rootNode === null) {
            return [];
        }


        // Get all leaves from this root node
        $levelChecker = $forceExactLevel ? '=' : '<=';
        $leavesQuery = $rootNode->children()
            ->with('pageRoles')
            ->andWhere([
                self::ATTR_ACTIVE => self::ACTIVE,
                self::ATTR_VISIBLE => self::VISIBLE,
                self::ATTR_ACCESS_DOMAIN => [self::GLOBAL_ACCESS_DOMAIN, mb_strtolower(\Yii::$app->language)],
            ])
            ->andWhere("lvl $levelChecker '$maxLevel'");
        if (!Yii::$app->user->can('pages')) {
            $leavesQuery->andWhere(
                [
                    self::ATTR_DISABLED => self::NOT_DISABLED,
                ]
            );
        }
        $key = md5($leavesQuery->createCommand()->getRawSql());
        $leaves = self::getCache()->get($key);
        if ($leaves === false) {
            $leaves = $leavesQuery->all();
            self::getCache()->set($key, $leaves, 0, self::getDependency());
        }
        if ($leaves === null) {
            return [];
        }

        // tree mapping and leave stack
        $treeMap = [];
        $stack = [];

        // Current page to determine active page
        $current = Tree::getCurrentItem();
        if ($current && $maxLevel < 3 && $current->lvl > 1) {
            // The selected page is of third level, but we are in the main menu, so we need to highlight the parent
            $current = $current->parents($current->lvl - 1)->one();
        }

        if (count($leaves) > 0) {
            foreach ($leaves as $page) {
                if ($page->lvl > $maxLevel) {
                    continue;
                }

                // prepare node identifiers
                $pageOptions = [
                    'data-page-id' => $page->id,
                    'data-lvl' => $page->lvl,
                ];

                $itemTemplate = [
                    'label' => ($page->icon) ? '<i class="' . $page->icon . '"></i> ' . $page->name : $page->name,
                    'url' => $page->createRoute(),
                    'linkOptions' => $pageOptions,
                    'visible' => ($checkUserPermissions) ?
                        Yii::$app->user->can(substr(str_replace('/', '_', $page->route), 1), ['route' => true]) :
                        true,
                ];

                // Add active class
                if (!empty($current) && $current->id == $page->id) {
                    $itemTemplate['active'] = true;
                }

                // Additional checks required on the page?
                $roles = $page->roles;
                if (!empty($roles) && $itemTemplate['visible']) {
                    $itemTemplate['visible'] = $page->hasAccess();
                }

                $item = $itemTemplate;

                // Count items in stack
                $counter = count($stack);

                // Check on different levels
                while ($counter > 0 &&
                    $stack[$counter - 1]['linkOptions']['data-lvl'] >= $item['linkOptions']['data-lvl']) {
                    array_pop($stack);
                    --$counter;
                }

                // Stack is now empty (check root again)
                if ($counter == 0) {
                    // assign root node
                    $i = count($treeMap);
                    $treeMap[$i] = $item;
                    $stack[] = &$treeMap[$i];
                } else {
                    if (!isset($stack[$counter - 1]['items'])) {
                        $stack[$counter - 1]['items'] = [];
                    }
                    // add the node to parent node
                    $i = count($stack[$counter - 1]['items']);
                    $stack[$counter - 1]['items'][$i] = $item;
                    $stack[] = &$stack[$counter - 1]['items'][$i];
                }
            }
        }

        return array_filter($treeMap);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageRoles()
    {
        return $this->hasMany(PageRole::className(), ['page_id' => 'id']);
    }

    /**
     * Update the roles attached to a page
     */
    protected function updateRoles()
    {
        $assignedItems = $this->getRoles();
        $assignedItemsNames = array_values($assignedItems);

        $newRoles = !empty($this->page_roles) ? array_values($this->page_roles) : [];

        // Build list of Role names to be removed.
        foreach (array_diff($assignedItemsNames, $newRoles) as $item) {
            if (empty($item)) {
                continue;
            }

            // Delete pageRole
            $oldRole = PageRole::findOne(['page_id' => $this->id, 'role' => $item]);
            if ($oldRole) {
                $oldRole->delete();
            }
        }

        // Build list of Role names to be added
        foreach (array_diff($newRoles, $assignedItemsNames) as $item) {
            $newRole = new PageRole();
            $newRole->page_id = $this->id;
            $newRole->role = $item;
            $newRole->save();
        }
    }

    /**
     * Check if we have at least one functioning access.
     * @param bool $checkParents
     * @return bool
     */
    public function hasAccess($checkParents = false)
    {
        $access = empty($this->pageRoles);

        // Only add conditions if the user can already access the route.
        foreach ($this->pageRoles as $role) {
            // As long as we have at least on access true, we can see the route.
            if (Yii::$app->user->can($role->role)) {
                $access = true;
            }
        }

        // Access? Check all parents. This will take a while.
        if ($access) {
            $query = $this->parents(1)->with('pageRoles');
            $key = md5($query->createCommand()->getRawSql());
            $parent = self::getCache()->get($key);
            if ($parent === false) {
                $parent = $query->one();
                self::getCache()->set($key, $parent, 0, self::getDependency());
            }
//            $parent = $this->parents(1)->with('pageRoles')->one();
            if (!empty($parent)) {
                // Will take car of checking all the parents.
                $access = $parent->hasAccess(1);
            }
        }

        return $access;
    }

    /**
     * @return null|object|\yii\caching\Cache
     * @throws \yii\base\InvalidConfigException
     */
    protected static function getCache()
    {
        if (self::$cache === null) {
            self::$cache = Yii::$app->get('menucache');
        }
        return self::$cache;
    }

    /**
     * Create a cache dependency
     * @return DbDependency
     */
    public static function getDependency()
    {
        $dependency = new DbDependency();
        $dependency->sql = 'SELECT MAX(updated_at) FROM dmstr_page';
        return $dependency;
    }
}
