<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 12/21/2016
 * Time: 7:08 PM
 */

namespace taktwerk\pages\models;

use \nullref\cms\models\Page as BasePage;
use nullref\cms\models\PageHasBlock;
use nullref\useful\behaviors\RelatedBehavior;
use yii\helpers\ArrayHelper;

class Page extends BasePage
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'related' => [
                'filedSuffix' => '_list',
                'class' => RelatedBehavior::className(),
                'indexBy' => 'id',
                'fields' => [
                    'items' => PageHasBlock::className(),
                ]
            ],
        ]);
    }
}
