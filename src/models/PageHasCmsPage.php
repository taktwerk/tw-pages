<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages\models;

use Yii;
use yii\db\ActiveRecord;
use taktwerk\pages\models\Tree;
use nullref\cms\models\Page as CmsPage;

/**
 * This is the model class for table "{{%dmstr_page_cms}}".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $cms_page_id
 *
 * @property PageHasCmsPage $pageHasCmsPage
 */
class PageHasCmsPage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dmstr_page_cms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'cms_page_id'], 'integer'],
            [['page_id', 'cms_page_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'cms_page_id' => 'Page CMS ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Tree::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmsPage()
    {
        return $this->hasOne(CmsPage::className(), ['id' => 'cms_page_id']);
    }
}
