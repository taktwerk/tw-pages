<?php
/**
 * Taktwerk Pages module, extends dmstr/pages.
 */
namespace taktwerk\pages\models;

use Yii;
use yii\db\ActiveRecord;
use taktwerk\pages\models\Tree as Page;
use nullref\cms\models\Page as CmsPage;

/**
 * This is the model class for table "{{%dmstr_page_cms}}".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $role
 *
 * @property PageHasCmsPage $pageHasCmsPage
 */
class PageRole extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],
            [['role'], 'string'],
            [['page_id', 'role'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'role' => 'Role',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}
