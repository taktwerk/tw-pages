<?php

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class m000000_000004_add_block_name extends Migration
{
    use MigrationTrait;

    public function up()
    {
        if (!$this->hasColumn('{{%cms_block}}', 'name')) {
            $this->addColumn('{{%cms_block}}', 'name', $this->string());
        }
        if (!$this->hasColumn('{{%cms_block}}', 'visibility')) {
            $this->addColumn('{{%cms_block}}', 'visibility', $this->smallInteger());
        }
    }

    public function down()
    {
        $this->dropColumn('{{%cms_block}}', 'name');
        $this->dropColumn('{{%cms_block}}', 'visibility');
        return true;
    }
}
