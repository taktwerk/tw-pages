<?php

use yii\db\Schema;
use yii\db\Migration;

class m160825_140000_page_roles extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%page_role}}',
            [
                'id' => Schema::TYPE_PK."",
                'page_id' => Schema::TYPE_INTEGER."(11) NOT NULL",
                'role' => Schema::TYPE_STRING . "(64) NOT NULL",
            ],
            $tableOptions
        );

        $this->createIndex('page_role', '{{%page_role}}','page_id',0);
    }

    public function down()
    {
        $this->dropTable('{{%page_role}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
