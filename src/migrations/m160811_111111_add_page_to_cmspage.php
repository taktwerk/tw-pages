<?php

use yii\db\Migration;

class m160811_111111_add_page_to_cmspage extends Migration
{
    public function up()
    {
        $this->createTable(
            'dmstr_page_cms',
            [
                'id' => $this->primaryKey(),
                'page_id' => $this->integer()->unsigned()->notNull(),
                'cms_page_id' => $this->integer()->unsigned()->notNull(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
            ]
        );
    }

    public function down()
    {
        $this->dropTable('dmstr_page_cms');
    }
}
