<?php

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class m000000_000006_add_columns_to_cms_page extends Migration
{
    use MigrationTrait;

    public function safeUp()
    {
        if (!$this->hasColumn('{{%cms_page}}', 'content')) {
            $this->addColumn('{{%cms_page}}', 'content', $this->text());
        }
        if (!$this->hasColumn('{{%cms_page}}', 'meta')) {
            $this->addColumn('{{%cms_page}}', 'meta', $this->text());
        }
        if (!$this->hasColumn('{{%cms_page}}', 'type')) {
            $this->addColumn('{{%cms_page}}', 'type', $this->smallInteger());
        }
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cms_page}}', 'content');
        $this->dropColumn('{{%cms_page}}', 'meta');
        $this->dropColumn('{{%cms_page}}', 'type');
        return true;
    }
}
