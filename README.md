Taktwerk Page Manager
=====================

Extends the dmstr/yii2-pages-module package.


Installation 
-------

Add the following in your modules config:
   
```json
'class' => 'taktwerk\pages\Module',
    'layout' => '@admin-views/layouts/main',
    'availableRoutes' => [
        '/site/index' => '/site/index',
    ],
], 
```
